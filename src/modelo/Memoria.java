package modelo;

import java.util.ArrayList;
import java.util.List;

public class Memoria {
    private enum TipoComando {
        ZERAR, NUMERO, DIV, MULT, SUB, SOMA, IGUAL, VIRGULA, SINAL;
    };
    private static final Memoria instancia = new Memoria();
    private final List<MemoriaObservador> observadores = new ArrayList<>();
    private String textoAtual = "";
    private String textoBuffer = "";
    private boolean substituir = false;
    private TipoComando ultimaOperacao;

    private Memoria() {
    }

    public static Memoria getInstancia() {
        return instancia;
    }

    public void adicionarObservador(MemoriaObservador o) {
        observadores.add(o);
    }

    public String getTextoAtual() {
        return textoAtual.isEmpty() ? "0" : textoAtual;
    }

    public void processarComando(String texto) {
        TipoComando tipo = detectarTipoComando(texto);

        if(tipo == null) {
            return;
        } else if(tipo == TipoComando.ZERAR) {
            textoAtual = "";
            textoBuffer = "";
            substituir = false;
            ultimaOperacao = null;
        } else if(tipo == TipoComando.SINAL) {
            textoAtual = textoAtual.contains("-") ? textoAtual.substring(1) : "-" + textoAtual;
        } else if(tipo == TipoComando.NUMERO || tipo == TipoComando.VIRGULA) {
            textoAtual = substituir ? texto : textoAtual + texto;
            substituir = false;
        } else {
            substituir = true;
            textoAtual = obterResultadoOperacao();
            textoBuffer = textoAtual;
            ultimaOperacao = tipo;
        }

        observadores.forEach(o -> o.valorAlterado(getTextoAtual()));
    }

    private String obterResultadoOperacao() {
        if(ultimaOperacao == null || ultimaOperacao == TipoComando.IGUAL) {
            return textoAtual;
        }
        double numeroBuffer = Double.parseDouble(textoBuffer.replace(",", "."));
        double numeroAtual = Double.parseDouble(textoAtual.replace(",", "."));
        double resultado = 0;

        if(ultimaOperacao == TipoComando.SOMA) {
            resultado = numeroBuffer + numeroAtual;
        } else if(ultimaOperacao == TipoComando.SUB) {
            resultado = numeroBuffer - numeroAtual;
        } else if(ultimaOperacao == TipoComando.MULT) {
            resultado = numeroBuffer * numeroAtual;
        } else if(ultimaOperacao == TipoComando.DIV) {
            resultado = numeroBuffer / numeroAtual;
        }
        String texto = Double.toString(resultado).replace(".", ",");
        boolean inteiro = texto.endsWith(",0");
        return inteiro ? texto.replace(",0", "") : texto;
    }

    private TipoComando detectarTipoComando(String texto) {
        if(textoAtual.isEmpty() && texto == "0") {
            return null;
        }

        try {
            Integer.parseInt(texto);
            return TipoComando.NUMERO;
        } catch (NumberFormatException e) {
            if(texto.equals("AC")) {
                return TipoComando.ZERAR;
            } else if(texto.equals("/")) {
                return TipoComando.DIV;
            } else if(texto.equals("*")) {
                return TipoComando.MULT;
            } else if(texto.equals("-")) {
                return TipoComando.SUB;
            } else if(texto.equals("+")) {
                return TipoComando.SOMA;
            } else if(texto.equals("=")) {
                return TipoComando.IGUAL;
            } else if(texto.equals("±")) {
                return TipoComando.SINAL;
            } else if(texto.equals(",") && !textoAtual.contains(",")) {
                return TipoComando.VIRGULA;
            }
        }
        return null;
    }
}